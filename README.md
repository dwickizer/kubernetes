# All Things Kubernetes

This repository is intended to hold helpful scripts and configuration guides I gather in my travels learning K8s.

In this repository you will find a few (hopefully) useful READMEs which teach you how to deploy Kubernetes (K8s), both manually and automatically. 


## Philosophy and How These Files Relate to One Another

These files are intended to move you from virtualization-specific to virtualization-independence (mostly)

* In other words, the **Gold Image** and tweaks to that image discussed below are virtualization-specific.

* The manual and automatic K8s install scripts can be used anywhere. They just assume you have a bunch of RHEL7 nodes. Those nodes could be VMs, AWS instances, or bare metal servers.

* I say "mostly". because the install scripts still do show Virtual Box specific access information to the nodes, once they are up and running.


								        +--------------------+                +----------------+
                                        |    Tweaked for     |                |   Manual K8s   |
						        +------>|   Manual Install   |--------------->|    Install     |
                                |       +--------------------+                +----------------+
		+------------------+    |
		| RHEL7 Gold Image |----+
		+------------------+    |  		
		                        |       +--------------------+                +----------------+
		                        +------>|    Tweaked for     |--------------->|  Automatic K8s |	
								        |  Automatic Install |                |    Install     |
										+--------------------+                +----------------+
										
		|<-------------- virtualization-specific ---------------->|<-- virtualization-independent -->|	
					


## RHEL 7 Gold Image

Both of these approaches start from a common **Gold Image**, based on RHEL7. The process for manaully building that image in Virtual Box, as well as the private NAT Network and port forwarding it uses is found here:

* [**Install-RHEL7-in-Vbox.md:**](https://bitbucket.org/dwickizer/kubernetes/src/master/Install-RHEL7-in-Vbox.md) This outlines the process for building a RHEL7 Gold Image (for use later in building a k8s cluster).

## Manually Deploying K8s

To manually deploy a small 3-node K8s cluster, start with these two READMEs:

* [**Linked-Clones-for-Manual-K8s-Install-README.md:**](https://bitbucket.org/dwickizer/kubernetes/src/master/Linked-Clones-for-Manual-K8s-Install-README.md) This provides instructions on how to tweak the Gold Image and create the three linked clones you will need.

* [**Manual-K8s-Install.md:**](https://bitbucket.org/dwickizer/kubernetes/src/master/Manual-K8s-Install.md) This provides instructions on how to manually build a 3-node, single master K8s cluster in Virtual Box using RHEL7 linked clones created in the step above.



## Automatically Deploying K8s Using Kubespray and Ansible

To automatically deploy a small 3-node K8s cluster and an Ansible server, start with these two READMEs:

* [**kubespray/Linked-Clones-for-Kubespray-Anisble-Install-README.md:**](https://bitbucket.org/dwickizer/kubernetes/src/master/kubespray/Linked-Clones-for-Kubespray-Anisble-Install-README.md) Instructions for creating linked clones from the Gold Image for Kubespray deployment.

* [**kubespray/Kubernetes Deployment Using Kubespray.md:**](https://bitbucket.org/dwickizer/kubernetes/src/master/kubespray/Kubernetes%20Deployment%20Using%20Kubespray.md) Instructions on how to automatically deploy a production K8s cluster using kubespray and ansible on the four linked clones created above.


## Some Simple K8s Deployments

Once your K8s cluster is up and running, here are some simple deployments from a Udacity course I took.

* [**Udacity-Course-Setup.md:**](https://bitbucket.org/dwickizer/kubernetes/src/master/Udacity-Course-Setup.md) This contains all the setup and commands run in the Udacity Course **Scalable Microservices with Kubernetes** by Kelsey Hightower from Google. You can skip down to the **auth.yaml**, **hello.yaml** and **frontend.yaml** examples.

* [**Deploy MySQL Server in a Pod**](https://bitbucket.org/dwickizer/kubernetes/src/master/examples/Deploying%20Mysql%20in%20a%20Pod.md)


## Other Related Stuff

* [**Install-Fake-Load-Balancer.md:**](https://bitbucket.org/dwickizer/kubernetes/src/master/Install-Fake-Load-Balancer.md) This shows you how to setup a fake load balancer for use with Virtual Box, to resolve the **pending** status in the **frontend.yaml** example above.

* [**Install-Kubernetes-Dashboard.md:**](https://bitbucket.org/dwickizer/kubernetes/src/master/Install-Kubernetes-Dashboard.md) This shows you how to setup and access the dashboard.


## To Dos:

* Replace the Virtual Box NAT Networking and the MetalLB fake load balancer with an **HA Proxy** reverse proxy and load balancer

* Automate the creation of the gold image, linked clones and HA Proxy server with Ansible and/or Vagrant

