# Create RHEL7 Gold Image on Virtual Box


## Background

* The gold image will be installed in a private NAT Network, which will use port forwarding for access. This network will be setup first

* A gold image, based on Redhat Enterprise Linux (RHEL) 7.6 will be used. 

	* Install RHEL
	* Setting Static IP Address
	* Change the hostnamne (if necessary)
	* Add Hostnames and IP Addresses
	* Configure Secure SSH
	* Configure sudo without password
	* Register the node with RHSM
	* Secure SSH access for rhel user
	* Snapshot the System (Optional)

## Creating the NAT Network

This assumes you have installed Virtual Box. If not, go [here](https://www.virtualbox.org/wiki/Downloads) and download the latest version of the software and its Extension Pack.

**NOTE:** I am using MacOS Mojave (10.14.2) Therefore, the menus mentioned in the application assume that platform.

* Launch Virtual Box.

* Click on **"VirtualBox VM > Preferences"**.

* Select **"Network"**.

* Click on the **"+"** on the right to add a new NAT Network. I called mine **"NATNetwork"** (how creative).

* Double-click on the NAT Network name you created.

* A modal will appear with the network name and CIDR address. Change the address to **10.0.2.0/24**.

* Check the **Supports DHCP** box. Leave the others unchecked.

* In a NAT Network you communicate through localhost (127.0.0.1) on your computer to talk to any VMs running on the network. To that requires that you set up port forwarding. For now, we want to enable SSH (port 22) traffic to specific IP addresses on the backend (referred to as the **Guest Port**). You will access each host through a unique port you enter on the front end (called the **Host Port**).

* Click on the **Port Forwarding** button.

* Click on the **"+"** button on the right to add each entry.

* Here are the entries I used:

```
    Name        Protocol   Host IP    Host Port      Guest IP    Guest Port
rhel-7.6-gold      TCP                  9622         10.0.2.6        22
k8s-master         TCP                  9722         10.0.2.7        22
k8s-node1          TCP                  9822         10.0.2.8        22
k8s-node2          TCP                  9922         10.0.2.9        22
```

* When you're done click **OK** on each of the open windows to save the settings. 

You're all set. Just remember to select this NAT Network when you configure the first VM.


## Install RHEL

* Download the latest ISO from Redhat.

* Click the **New** button

* Give it an name. I used **rhel-7.6-gold**.

* Select **Linux** for the type.

* Select **Redhad (64-bit)** for the Version.

* Click **Continue**.

* Choose **2048 MB** for the amount of RAM and click **Continue**.

* Select **Create a virtual hard disk now** and click **Continue**.

* Select **VDI** and click **Continue**.

* Select **Dynamically allocated** and click **Continue**.

* Suggest you use **20 GB** and click **OK**.


You now have a VM created in the Virtual Box menu. Before starting it, some more configuration is required.

* Click once to select the VM you created.

* Select **Machine > Settings** from the menu at the top (or click on the **Settings** button).

* Under the **General > Description** tab, it is a good practice to enter information here about the VM (IP addresses, usernames, etc.)

* Under the **System > Processor** tab, make sure you give it **2 CPUs**.

* Under the **Storage** tab, click on the **Empty** DVD icon under **Controller: IDE**.

* Click on the **DVD Icon** to the right of **Optical Drive: IDE Secondary Master**.

* Select **Choose Virtual Optical Disk File** and point it to the RHEL ISO file you downloaded from Redhat. Select **Open**

* Under the **Network** tab, ensure that **Adapter 1** is enabled and choose **NAT Network** in the pulldown.

* Choose the name of the NAT Network you created above. 

* Click **OK** to save all these settings.


You're now ready to launch your VM and begin installation.

* Click once to select your VM in the menu and click the **Start** button above.

* An installation window will open. Click on the line that says, **Install Redhat Enterprise Linux 7.6** (or whatever version you are using) and hit RETURN.

* Select the language and hit **Continue**.

* Next you'll see a window where you will need to configure a few things:

	* Click **Installation Destination**. It will open another window. The disk you created should be selected. We'll use auto partitioning. Click **Done**.
	
	* Click **Network & Host Name**. 
	
		* Click the **ON** button to enable the adapter
		
		* Give it the host name you want at the bottom. I used **rhel-7.6-gold.raas.net**.
		
		* Click **Done**.
		
	
	* Click **Software Selection**. A window will open. 
	
		* Select **Minimal Install** on the left
		
		* Select **Development Tools, Security Tools** and **System Administration Tools** on the right.
		
		* Click **Done**.
		
		
	* Click **Security Policy**. This will open up another window allowing you to select and enable the appropriate security policy
	
		* I choose the **United States Government Configuration Baseline** configuration
		
		* Click on the **Select Profile** below the window.
		
		* Make sure the **Apply Button** is **On**.
		
		* Click **Done**.
		
		
	* Click **Begin Installation**.
	
	
* In the next window you will be given the opportunity to set the root password and create a user.

	* Set the root password
	
	* Create a user
	
		* Give the user a name. (I used **rhel**)
	
		* Check **Make this user adminstrator**.
		
		* Give the user a password.
		
		* Select **Done**.
		
		
* Once everything is installed, click **Finish Installation**.

* Click **Reboot** when everything is done.

## Setting Static IP Address

* Login as the privileged user you created. (**Hint:** You might need to login to the console first and find out what the IP address is, then create an appropriate port forwarding entry in the NAT network you created above.) In my case, it was assigned 10.0.2.4. So, I created a port mapping from port 9422 on localhost to 10.0.2.4 port 22.

```
ssh -p 9422 rhel@localhost`
```


* Become superuser (root)

```
sudo su
```

    Enter your password


* Create a file named /etc/sysconfig/network-scripts/ifcfg-enp0s3 as follows: 

**NOTE:** The device name for my network interface is enp0s3. Yours might be different.
 
```
cat <<EOF > /etc/sysconfig/network-scripts/ifcfg-enp0s3
	TYPE=Ethernet
	BOOTPROTO=none
	# Server IP
	IPADDR=10.0.2.6
	#Subnet
	PREFIX=24
	# Set default gateway IP
	GATEWAY=10.0.2.1
	#Set dns servers
	DNS1=10.0.2.1
	DNS2=8.8.8.8
	DNS3=8.8.4.4
	DEFROUTE=yes
	IPV4_FAILURE_FATAL=no
	# Disable ipv6
	IPV6INIT=no
	IPV6_PRIVACY=rfc3041
	NAME=enp0s3
	# This is system specific and can be created using 'uuidgen enp0s3' command
	UUID=436fd6a6-829b-4caf-882e-1ddb360602f9
	DEVICE=enp0s3
	ONBOOT=yes
EOF
```


**NOTE when you create linked clones from this Gold Image:** You can run the following commands to create a new UUID, after changing the IP address in the file above:

```
OLD_UUID="`grep UUID /etc/sysconfig/network-scripts/ifcfg-enp0s3 | cut -d= -f2`"
NEW_UUID="`uuidgen enp0s3`" 
sed -i "s/$OLD_UUID/$NEW_UUID/" /etc/sysconfig/network-scripts/ifcfg-enp0s3
```


## Changing Hostname (Permanently), If Necessary

* If you messed up the hostname during installation, you can change it now.
 
```
sudo hostnamectl set-hostname <name>
```


## Configuring Secure SSH Access

* Execute the following commands to lock down SSH. (Don't allow root login at all; don't allow password login; ignore comments)
 
```
sudo sed -i '/^#/!s/PermitRootLogin yes/PermitRootLogin no/g' /etc/ssh/sshd_config
sudo sed -i '/^#/!s/PasswordAuthentication yes/PasswordAuthentication no/g' /etc/ssh/sshd_config
```

* Restart network services
 
```
sudo systemctl restart sshd
```


## Configure sudo without password

The **rhel** user was already made part of the **wheel** group during installation, when the account was created as an admin user.
So, just uncomment the version in sudoers with the NOPASSWD in it (it is commented out by default).

```
sudo cp /etc/sudoers /etc/sudoers.bak
sudo sed -Ei "s/^(# %wheel)/%wheel/" /etc/sudoers
```

## Register the node with Redhat RHSM


```
sudo subscription-manager register --auto-attach
```

    # Response		
	Registering to: subscription.rhsm.redhat.com:443/subscription		
	Username: <type your RHSM username>	
	Password: <type your RHSM password>	
	The system has been registered with ID: da011c27-7304-4b45-8aab-927e800a5ef	
	The registered system name is: rhel-7.6-gold.raas.net		
	Product Name: Red Hat Enterprise Linux Server		
	Status:       Subscribed


## Update packages

* **NOTE:** This could take a while, but it will save you time later when you use the image to build other systems.

```
sudo yum upgrade -y
```

* While we're at it, a couple of other helpful tools:

```
sudo yum install -y wget net-tools
```


## Secure SSH access for rhel user

* On the system you want to use to access the server create an rsa key file by running 
   
```
ssh-keygen
```	

* Give the key a name (e.g., $HOME/.ssh/my\_id\_rsa)

* Follow prompts
	
* **Important:** Change the permissions on the private key file you created:	 

```
chmod 600 $HOME/.ssh/my_id_rsa
```


* Back on the server - Create a .ssh directory in your home account (make sure you are NOT root)
 
```
mkdir -p $HOME/.ssh
chmod 750 $HOME/.ssh
```


* Back on your local system - Edit the public key file you created and copy the contents onto the clipboard:

```
 vi $HOME/.ssh/my_id_rsa.pub
 # Copy contents
 # Quit
 :q
```

* Back on the server - Create authorized\_keys file (as the non-root user you will use to access the system), paste the public key and save it

```
 vi $HOME/.ssh/authorized_keys
 #Insert mode
 i
 # Paste in public key
 # Hit ESC key to stop editing
 ESC 
 # Save changes and exit
 :wq or ZZ 
```

* Change the permissions to readony for the authorized\_keys file
 
```
chmod 400 $HOME/.ssh/authorized_keys
```


* You should now have Secure SSH access from your local system. **NOTE:** This is what I use in Virtualbox:
 
    ssh -i $HOME/.ssh/my_id_rsa -p $PORT <remoteuser>@localhost


```
ssh -i $HOME/.ssh/my_id_rsa -p 9622 rhel@localhost
```


## Snapshot the System (Optional)

* At this point I recommend you shutdown the system and snapshot it as your baseline install, just in case you have trouble with the next section and want a do-over.
 
```
sudo shutdown -h now
```

* Highlight your VM in the Virtual Box Window (once the system is shutdown)

* Click on the **Snapshots** button in the upper right

* Click on the **Take** button

* A modal will pop-up. Name the snapshot and provide any descriptive information you want, then click **OK**.

`Snapshot Name`		

`RHEL-7.6-Baseline-01-17-2019`

`Snapshot Description`		

```
Installed OS		
network configured		
hostnamectl configured	
ssh locked down to keys only	
static IP and network configured
```


**Your gold image is done!**	


 	



