# Run the Kubernetes Dashboard

The good news is that Kubernetes has a web dashboard which provides a good high level overview of the cluster. The bad news is that it is a pain to configure. The steps below will show you how. For details on how to configure it, look [here to deploy the dashboard](https://kubernetes.io/docs/tasks/access-application-cluster/web-ui-dashboard/) and [here to configure a user to login to the dashboard](https://github.com/kubernetes/dashboard/wiki/Creating-sample-user)


## Deploy and start the dashboard proxy

* To deploy the dashboard, run the following command:
 
`kubectl create -f https://raw.githubusercontent.com/kubernetes/dashboard/master/aio/deploy/recommended/kubernetes-dashboard.yaml`

* To make it talk to the outside world (or at least our NAT network to begin with), run the following:
 
`kubectl proxy --address='10.0.2.7'&`
	
* **NOTE:** This will run a web proxy at 10.0.2.7 port 8001 (by default). You can specify a different port by using the --port=<port number> option.


## Create the port map in the Virtual Box NAT networking

* To get access to the outside world, you'll need to create another port map in Virtual Box networking

	* Select **VirtualBox VM > Perferences...**
	
	* Click on the **Network** button at the top
	
	* Double-click on the NAT network name you created
	
	* Click on the **Port Forwarding** button
	
	* Add the following entry and click **OK** in all the modals to save the entry:
 
```
	      Name     Protocol  Host IP    Host Port   Guest IP  Guest Port	
	kube-dashboard    TCP                 8001      10.0.2.7    8001
```

## Test the dashboard

* To test that you have access to the dashboard, open up a browswer and enter the following:
 
`http://localhost:8001/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy/#!/login`

* A web page should appear asking you to either use Kubeconfig or Enter a token. Hold that thought.


## Create a dashboard user

* You will need to create a privileged user to access the dashboard

* Create the user by first creating a YAML file describing the user:

```
cat <<EOF > ./dashboard-admin-user.yaml
apiVersion: v1
kind: ServiceAccount
metadata:
  name: admin-user
  namespace: kube-system
EOF	
```

* Deploy the user:
 
`kubectl apply -f ./dashboard-admin-user.yaml`
	
	# Response you should see:
	
	serviceaccount/admin-user created



## Map user to a role

* Next you will need to create a cluster role and map that role to the admin-user:

```
cat <<EOF > ./cluster-role-binding.yaml
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: admin-user
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
- kind: ServiceAccount
  name: admin-user
  namespace: kube-system
EOF
```

* Next deploy the role and mapping
 
`kubectl apply -f ./cluster-role-binding.yaml`
	
	# Response you should see:
	
	clusterrolebinding.rbac.authorization.k8s.io/admin-user created



## Find the user bearer token

* To find the bearer token you will need to login to the dashboard, run the following command:
 
`kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep admin-user | awk '{print $1}')`
	
	# Response
	
	Name:         admin-user-token-rt9rb
	Namespace:    kube-system
	Labels:       <none>
	Annotations:  kubernetes.io/service-account.name: admin-user
	              kubernetes.io/service-account.uid: ed43c317-1b57-11e9-bb89-0800277d1dfd

	Type:  kubernetes.io/service-account-token

	Data
	====
	namespace:  11 bytes
	token:      eyJhbGciOiJSUzI1NiIsImtpZCI6IiJ9.eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9uYW1lc3BhY2UiOiJrdWJlLXN5c3RlbSIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VjcmV0Lm5hbWUiOiJhZG1pbi11c2VyLXRva2VuLXJ0OXJiIiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9zZXJ2aWNlLWFjY291bnQubmFtZSI6ImFkbWluLXVzZXIiLCJrdWJlcm5ldGVzLmlvL3NlcnZpY2VhY2NvdW50L3NlcnZpY2UtYWNjb3VudC51aWQiOiJlZDQzYzMxNy0xYjU3LTExZTktYmI4OS0wODAwMjc3ZDFkZmQiLCJzdWIiOiJzeXN0ZW06c2VydmljZWFjY291bnQ6a3ViZS1zeXN0ZW06YWRtaW4tdXNlciJ9.cRiXT1CN0SZ2bOt-_4OR10Mq2JaujCFMB0nYnYRsSGIkbwKgzDsKa2G7xuXyWAqm_aIRqHYoOVPHeWAzEtcaBmXjLUriFrPha1LftQtVi7w5u_lnZEHB74cpJOFCF_q6J431gvI8Qv0VaE0CJKbFtCliwJEbOzpr94wXUG09I6iVZ3bWt9o4ZLZhBeu4ey3YgYGfZP0apmhJHS9THJIov6filMn7ZqiFtVmMetkzHGU3C2M1yM-VKcxzqYNawGOnxNYXNRaomb3Kl4cRixRPJ3xOz3Mhu32UXI-_A3TDWA1On77GbpI0T9yF6e_nsK8NBU_SHzRB7SwlJc_Mxt4mww
	ca.crt:     1025 bytes


## Login with the bearer token

* Copy the **token** from above 

* Paste it into the browser and click the **Sign-in** button.



