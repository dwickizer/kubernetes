# Manual Install of Kubernetes Cluster on RHEL7 Nodes


## Background

The purpose of this guide is help you manually install a 3-node Kubernetes (K8s) cluster (one master and two slave nodes) in RHEL7. Three RHEL7 nodes are assumed to be available. **NOTE:** [Here is a process for creating these manually as linked clones in Virtual Box.](https://bitbucket.org/dwickizer/kubernetes/src/master/Linked-Clones-for-Manual-K8s-Install-README.md). The cluster will be installed in a private NAT Network, which will use port forwarding for access. [(If you are using Virtual Box, this process tells you how to create a private NAT Network)](https://bitbucket.org/dwickizer/kubernetes/src/master/Install-RHEL7-in-Vbox.md). 

Here is the process for manually installing K8s:

* Configure Kubernetes Master Server

	* Login to the K8s master
	* Pull initial images
	* Initialize cluster
	* Configure admin user
	* Deploy a pod network add-on	
	
* Configure Kubernetes Nodes

	* Login to each k8s node
	* Join nodes to the cluster
	* Repeat as necessary

* Exposing Kubernetes Services Through the NAT Network


## Configure Kubernetes Master Server

### Login to k8s master

 
```
ssh -i $HOME/.ssh/my_id_rsa -p 9722 rhel@localhost
```


### Pull initial images
 
```
sudo kubeadm config images pull
```

	# Response will look similar to this:
	
	[config/images] Pulled k8s.gcr.io/kube-apiserver:v1.14.0
	[config/images] Pulled k8s.gcr.io/kube-controller-manager:v1.14.0
	[config/images] Pulled k8s.gcr.io/kube-scheduler:v1.14.0
	[config/images] Pulled k8s.gcr.io/kube-proxy:v1.14.0
	[config/images] Pulled k8s.gcr.io/pause:3.1
	[config/images] Pulled k8s.gcr.io/etcd:3.3.10
	[config/images] Pulled k8s.gcr.io/coredns:1.3.1


### Initialize cluster

``` 
sudo kubeadm init --apiserver-advertise-address=10.0.2.7 --pod-network-cidr=10.0.0.0/16
```

	
	# Response will look similar to this:
	
	[init] Using Kubernetes version: v1.13.2
	[preflight] Running pre-flight checks
		[WARNING Firewalld]: firewalld is active, please ensure ports [6443 10250] are open or your cluster may not function correctly
	[preflight] Pulling images required for setting up a Kubernetes cluster
	[preflight] This might take a minute or two, depending on the speed of your internet connection
	[preflight] You can also perform this action in beforehand using 'kubeadm config images pull'
	[kubelet-start] Writing kubelet environment file with flags to file "/var/lib/kubelet/kubeadm-flags.env"
	[kubelet-start] Writing kubelet configuration to file "/var/lib/kubelet/config.yaml"
	[kubelet-start] Activating the kubelet service
	[certs] Using certificateDir folder "/etc/kubernetes/pki"
	[certs] Generating "etcd/ca" certificate and key
	[certs] Generating "etcd/server" certificate and key
	[certs] etcd/server serving cert is signed for DNS names [k8s-master localhost] and IPs [10.0.2.7 127.0.0.1 ::1]
	[certs] Generating "etcd/peer" certificate and key
	[certs] etcd/peer serving cert is signed for DNS names [k8s-master localhost] and IPs [10.0.2.7 127.0.0.1 ::1]
	[certs] Generating "etcd/healthcheck-client" certificate and key
	[certs] Generating "apiserver-etcd-client" certificate and key
	[certs] Generating "ca" certificate and key
	[certs] Generating "apiserver" certificate and key
	[certs] apiserver serving cert is signed for DNS names [k8s-master kubernetes kubernetes.default kubernetes.default.svc kubernetes.default.svc.cluster.local] and IPs [10.96.0.1 10.0.2.7]
	[certs] Generating "apiserver-kubelet-client" certificate and key
	[certs] Generating "front-proxy-ca" certificate and key
	[certs] Generating "front-proxy-client" certificate and key
	[certs] Generating "sa" key and public key
	[kubeconfig] Using kubeconfig folder "/etc/kubernetes"
	[kubeconfig] Writing "admin.conf" kubeconfig file
	[kubeconfig] Writing "kubelet.conf" kubeconfig file
	[kubeconfig] Writing "controller-manager.conf" kubeconfig file
	[kubeconfig] Writing "scheduler.conf" kubeconfig file
	[control-plane] Using manifest folder "/etc/kubernetes/manifests"
	[control-plane] Creating static Pod manifest for "kube-apiserver"
	[control-plane] Creating static Pod manifest for "kube-controller-manager"
	[control-plane] Creating static Pod manifest for "kube-scheduler"
	[etcd] Creating static Pod manifest for local etcd in "/etc/kubernetes/manifests"
	[wait-control-plane] Waiting for the kubelet to boot up the control plane as static Pods from directory "/etc/kubernetes/manifests". This can take up to 4m0s
	[apiclient] All control plane components are healthy after 19.016318 seconds
	[uploadconfig] storing the configuration used in ConfigMap "kubeadm-config" in the "kube-system" Namespace
	[kubelet] Creating a ConfigMap "kubelet-config-1.13" in namespace kube-system with the configuration for the kubelets in the cluster
	[patchnode] Uploading the CRI Socket information "/var/run/dockershim.sock" to the Node API object "kube-master" as an annotation
	[mark-control-plane] Marking the node kube-master as control-plane by adding the label "node-role.kubernetes.io/master=''"
	[mark-control-plane] Marking the node kube-master as control-plane by adding the taints [node-role.kubernetes.io/master:NoSchedule]
	[bootstrap-token] Using token: 4mink5.7edfttvt1j2agmq1
	[bootstrap-token] Configuring bootstrap tokens, cluster-info ConfigMap, RBAC Roles
	[bootstraptoken] configured RBAC rules to allow Node Bootstrap tokens to post CSRs in order for nodes to get long term certificate credentials
	[bootstraptoken] configured RBAC rules to allow the csrapprover controller automatically approve CSRs from a Node Bootstrap Token
	[bootstraptoken] configured RBAC rules to allow certificate rotation for all node client certificates in the cluster
	[bootstraptoken] creating the "cluster-info" ConfigMap in the "kube-public" namespace
	[addons] Applied essential addon: CoreDNS
	[addons] Applied essential addon: kube-proxy
	
	Your Kubernetes master has initialized successfully!
	
	To start using your cluster, you need to run the following as a regular user:
	
	  mkdir -p $HOME/.kube
	  sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
	  sudo chown $(id -u):$(id -g) $HOME/.kube/config
	  
	You should now deploy a pod network to the cluster.
	Run "kubectl apply -f [podnetwork].yaml" with one of the options listed at:
	  https://kubernetes.io/docs/concepts/cluster-administration/addons/
	  
	You can now join any number of machines by running the following on each node
	as root:
	
	  kubeadm join 10.0.2.7:6443 --token 4mink5.7edfttvt1j2agmq1 --discovery-token-ca-cert-hash sha256:b560a27256835f88cfb656da864ef8b9e728b1d60d5d5bbf78a90ab08b3abf91
	

* **IMPORTANT:** Save the join string somewhere. You will need it later!


### Configure admin user

* Now follow the other directions above that, starting with configuring the non-root user account to manage the cluster:

``` 
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
```

### Deploy a pod network add-on

* You should now deploy a pod network to the cluster.
 
``` 
kubectl apply -f [podnetwork].yaml"
``` 

* Where [podnetwork].yaml is one of the options listed [here](https://kubernetes.io/docs/concepts/cluster-administration/addons/)

* Make sure NetworkManager does not interfere with calico:

```  
sudo su -c 'cat << EOF > /etc/NetworkManager/conf.d/calico.conf
[keyfile]
unmanaged-devices=interface-name:cali*;interface-name:tunl*
EOF'
```  


* Install Calico:

```  
kubectl apply -f https://docs.projectcalico.org/v3.6/getting-started/kubernetes/installation/hosted/kubernetes-datastore/calico-networking/1.7/calico.yaml
```  


    # You should see something similar to the following output:	
		
	configmap/calico-config created
	customresourcedefinition.apiextensions.k8s.io/felixconfigurations.crd.projectcalico.org created
	customresourcedefinition.apiextensions.k8s.io/ipamblocks.crd.projectcalico.org created
	customresourcedefinition.apiextensions.k8s.io/blockaffinities.crd.projectcalico.org created
	customresourcedefinition.apiextensions.k8s.io/ipamhandles.crd.projectcalico.org created
	customresourcedefinition.apiextensions.k8s.io/ipamconfigs.crd.projectcalico.org created
	customresourcedefinition.apiextensions.k8s.io/bgppeers.crd.projectcalico.org created
	customresourcedefinition.apiextensions.k8s.io/bgpconfigurations.crd.projectcalico.org created
	customresourcedefinition.apiextensions.k8s.io/ippools.crd.projectcalico.org created
	customresourcedefinition.apiextensions.k8s.io/hostendpoints.crd.projectcalico.org created
	customresourcedefinition.apiextensions.k8s.io/clusterinformations.crd.projectcalico.org created
	customresourcedefinition.apiextensions.k8s.io/globalnetworkpolicies.crd.projectcalico.org created
	customresourcedefinition.apiextensions.k8s.io/globalnetworksets.crd.projectcalico.org created
	customresourcedefinition.apiextensions.k8s.io/networkpolicies.crd.projectcalico.org created
	clusterrole.rbac.authorization.k8s.io/calico-kube-controllers created
	clusterrolebinding.rbac.authorization.k8s.io/calico-kube-controllers created
	clusterrole.rbac.authorization.k8s.io/calico-node created
	clusterrolebinding.rbac.authorization.k8s.io/calico-node created
	daemonset.extensions/calico-node created
	serviceaccount/calico-node created
	deployment.extensions/calico-kube-controllers created
	serviceaccount/calico-kube-controllers created



* Confirm that all the pods are running with the following command:
 
```
kubectl get pods --all-namespaces
```

			
	# Eventually you should see all the pods running:
	
	NAMESPACE     NAME                                       READY   STATUS    RESTARTS   AGE
	kube-system   calico-etcd-xcjfg                          1/1     Running   0          18s
	kube-system   calico-kube-controllers-5d94b577bb-jb6r6   1/1     Running   0          31s
	kube-system   calico-node-4hjld                          1/1     Running   1          31s
	kube-system   coredns-86c58d9df4-89cmt                   1/1     Running   0          12m
	kube-system   coredns-86c58d9df4-l6v52                   1/1     Running   0          12m
	kube-system   etcd-kube-master                           1/1     Running   0          11m
	kube-system   kube-apiserver-kube-master                 1/1     Running   0          11m
	kube-system   kube-controller-manager-kube-master        1/1     Running   0          11m
	kube-system   kube-proxy-mp2ll                           1/1     Running   0          12m
	kube-system   kube-scheduler-kube-master                 1/1     Running   0          11m


* Remove taints on master so you can schedule pods on it

```
kubectl taint nodes --all node-role.kubernetes.io/master-
```

    # Expected response:
	node/k8s-master untainted

* Install calicoctl service on the master node:

* Create configuration directory

```
sudo mkdir -p /etc/calico
```

* Create default configuration file, so it can talk to the kubernetes API datastore

```
sudo su -c 'cat << EOF > /etc/calico/calicoctl.cfg
apiVersion: projectcalico.org/v3
kind: CalicoAPIConfig
metadata:
spec:
  datastoreType: "kubernetes"
  kubeconfig: "/home/rhel/.kube/config"
EOF'
```

* Install calicoctl in /usr/local/bin (which is in your PATH)

```
cd /usr/local/bin
sudo curl -O -L  https://github.com/projectcalico/calicoctl/releases/download/v3.6.1/calicoctl
sudo chmod +x calicoctl
```

* Test that it is installed correctly:

```
calicoctl get nodes -o wide
```

    # Expected response
	NAME         ASN         IPV4          IPV6   
	k8s-master   (unknown)   10.0.2.7/24          

## Configure Kubernetes Nodes


### Login to each K8s node

``` 
ssh -i $HOME/.ssh/my_id_rsa -p 9822 rhel@localhost
```

### Join nodes to the cluster

* Example cluster join command:
 
```
sudo kubeadm join 10.0.2.7:6443 --token 4mink5.7edfttvt1j2agmq1 --discovery-token-ca-cert-hash sha256:b560a27256835f88cfb656da864ef8b9e728b1d60d5d5bbf78a90ab08b3abf91
```

**NOTE:** Don’t worry if token will expire kubeadm token create and kubeadm token list will help create new tokens for join nodes and login to dashboard.


### Repeat as necessary

* I did one more called **k8s-node2** at IP address **10.0.2.9**


* Verify cluster from the master node
 
```
kubectl get nodes -o wide
```

	
	# Response should look like this
	
	NAME         STATUS   ROLES    AGE     VERSION   INTERNAL-IP   EXTERNAL-IP   OS-IMAGE                   KERNEL-VERSION               CONTAINER-RUNTIME
	k8s-master   Ready    master   47m     v1.14.0   10.0.2.7      <none>        Red Hat Enterprise Linux   3.10.0-957.10.1.el7.x86_64   docker://18.6.1
	k8s-node1    Ready    <none>   9m18s   v1.14.0   10.0.2.8      <none>        Red Hat Enterprise Linux   3.10.0-957.10.1.el7.x86_64   docker://18.6.1
	k8s-node2    Ready    <none>   2m57s   v1.14.0   10.0.2.9      <none>        Red Hat Enterprise Linux   3.10.0-957.10.1.el7.x86_64   docker://18.6.1


## Exposing Kubernetes Services Through the NAT Network

At some point you are going to want to access applications which have been exposed as a K8s service (e.g., to display a web page on the host system).

To do this, login to the **master server** and determine the port where the service is running.

```
kubectl get services
```

	
	# Response should look something like this:
	
	NAME           TYPE           CLUSTER-IP       EXTERNAL-IP   PORT(S)          AGE
	guestbook      LoadBalancer   10.105.177.68    10.96.0.200   3000:30891/TCP   166m
	kubernetes     ClusterIP      10.96.0.1        <none>        443/TCP          40d
	redis-master   ClusterIP      10.111.223.227   <none>        6379/TCP         166m
	redis-slave    ClusterIP      10.101.195.63    <none>        6379/TCP         166m

In this case we're interested in the "guestbook" service. Notice that this is addressable at 10.96.0.200 port 3000. However, this address is not reachable from the Virtual Box host, but it is available on the k8s master node IP address, which in our case is 10.0.2.7 port 30891 (the port to which port 3000 maps).

Furthermore, it is not just available on that IP address and port, it is available on **ALL** the k8s nodes at port 30891 via the K8s proxy service.

To expose it to our Virtual Box host, go the **VirtualBox > Preferences**, then double-click on **Network**, double-click on the NAT network name, lick on **Port Forwarding**.

You will see a table of port mappings. Click on the **"+"** on the upper right to add the following entry:

	   Name        Protocol       Host IP      Host Port    Guest IP      Guest Port
	guestbook        TCP           3001                     10.0.2.0       30891

Click on **"OK"** in all the modals to save this.

Now open a browser on your Virtual Box host and enter the following:

```
http://localhost:3001
```

The web page of the application should appear.

