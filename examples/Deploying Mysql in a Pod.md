#Deploying MySQL in a Pod

* **References:**

  * [**Basic Steps for MySQL Server Deployment with Docker**](https://dev.mysql.com/doc/refman/8.0/en/docker-mysql-getting-started.html)
  
  * [**Run a Single-Instance Stateful Application**](https://kubernetes.io/docs/tasks/run-application/run-single-instance-stateful-application/)
  

##Assumptions

* This assumes the following kubernetes directory structure:

```
kubernetes---+---/configmaps
             |
			 +---/deployments
			 |
			 +---/nginx
			 |
			 +---/pods
			 |
			 +---/secrets
			 |
			 +---/services
			 |
			 +---/tls
```

##Create a Secret for the Database Username & Password

* Create a config file with your database username and password in it. **NOTE:** Do not allow this to be stored in source code management.

```
cat << EOF > ./db_config.yaml
username: "root"
password: "<your password>"
EOF
```

  Where **<your password>** is the root database password you will be using.


* Generate secrets configuration file (secrets/mysql-secret.yaml)

```
cat << EOF > secrets/mysql-secret.yaml
apiVersion: v1
kind: Secret
metadata:
  name: mysql-secret
type: Opaque
stringData:
  db_config.yaml: |-
    username: {{ username }}
    password: {{ password }}
EOF
```


* Create secret using kubectl

```
kubectl create -f secrets/mysql-secret
```

	# Response should be 
	
	secret "mysql-secret" created


##Create and Deploy the MySQL Deployment

* Create a MySQL deployment configuration file using the secret created above and a persistent volume of 100 MiB 

```
cat << EOF > deployments/mysql.yaml
apiVersion: v1
kind: PersistentVolume
metadata:
  name: mysql-pv-volume
  labels:
    type: local
spec:
  storageClassName: manual
  capacity:
    storage: 100Mi
  accessModes:
  - ReadWriteOnce
  hostPath:
    path: "/mnt/data"
---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: mysql-pv-claim
spec:
  storageClassName: manual
  accessModes:
  - ReadWriteOnce
  resources:
    requests:
      storage: 100Mi
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: mysql
spec:
  selector:
    matchLabels:
      app: mysql
  strategy:
    type: Recreate
  template:
    metadata:
      labels:
        app: mysql
    spec:
      containers:
      - image: "mysql/mysql-server:8.0"
        name: mysql
        ports:
        - containerPort: 3306
          name: mysql
        volumeMounts:
        - name: mysql-secret-volume
          mountPath: /etc/mysql-secret-volume
        - name: mysql-persistent-storage
          mountPath: /var/lib/mysql
      # The secret data is exposed to Containers in the Pod through a Volume.
      volumes:
      - name: mysql-secret-volume
        secret:
          secretName: mysql-secret
      - name: mysql-persistent-storage
        persistentVolumeClaim:
          claimName: mysql-pv-claim
EOF
```

* Deploy mysql:

```
kubectl create -f deployments/mysql.yaml
```

    # Response should be:
	
	persistentvolume/mysql-pv-volume created
	persistentvolumeclaim/mysql-pv-claim created
	deployment.apps/mysql created

* Verify the pod is running

```
kubectl get pods -l app=mysql -o wide
```

    # Response should be something like this
	
	NAME                     READY   STATUS    RESTARTS   AGE     IP                NODE        NOMINATED NODE   READINESS GATES
	mysql-6b94b57ff5-sj8zr   1/1     Running   0          8m28s   192.168.169.148   k8s-node2   <none>           <none>

* Verify the volume claim

```
kubectl describe pvc mysql-pv-claim
```

    # Response should be something like this
	
	Name:          mysql-pv-claim
	Namespace:     default
	StorageClass:  manual
	Status:        Bound
	Volume:        mysql-pv-volume
	Labels:        <none>
	Annotations:   pv.kubernetes.io/bind-completed: yes
	               pv.kubernetes.io/bound-by-controller: yes
	Finalizers:    [kubernetes.io/pvc-protection]
	Capacity:      100Mi
	Access Modes:  RWO
	VolumeMode:    Filesystem
	Events:        <none>
	Mounted By:    mysql-6b94b57ff5-sj8zr
	


##Create and Deploy the MySQL Service

* Create the mysql service file:

```
cat << EOF > services/mysql.yaml
apiVersion: v1
kind: Service
metadata:
  name: mysql
spec:
  selector:
    app: mysql
  ports:
  - protocol: "TCP"
    port: 3306
    targetPort: 3306
  type: NodePort
EOL
```

* Deploy the mysql service:

```
kubectl create -f services/mysql.yaml
```

    # Response should be
	
	service/mysql created


##Access the Pod Using MySQL and Reset Password for Root

* When mysql was deployed a temporary root password was created. Find out what it was. (You'll need the pod name.)

```
kubectl logs mysql-6b94b57ff5-sj8zr | grep ROOT
```

    # Response should be something like this
	
	[Entrypoint] GENERATED ROOT PASSWORD: S4nSif%Eh2Eq3xqIHel[IGOcrOP


* Use it to login directly to the pod and reset root password

```
kubectl exec -it mysql-6b94b57ff5-sj8zr -- mysql -p
```

    # Response will be
	
	Enter password: 
	
	# Paste in the password above and hit RETURN
	
	Welcome to the MySQL monitor.  Commands end with ; or \g.
	Your MySQL connection id is 10
	Server version: 8.0.15
     
	Copyright (c) 2000, 2019, Oracle and/or its affiliates. All rights reserved.
    
	Oracle is a registered trademark of Oracle Corporation and/or its
	affiliates. Other names may be trademarks of their respective
	owners.
    
	Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.
    
	mysql>

* Reset the password for root (@localhost)

```
ALTER User 'root'@'localhost' IDENTIFIED BY '<your new password>';
```
Where <your password> is the same as the one used when you created the secret above.


    # Response should be:
	
	Query OK, 0 rows affected (0.01 sec)


* **OPTIONAL:** Normally, you would only access using root through the local DB pod. Access from other pods would be for a specific database, by a limited user account. Here, for demo purposes you can also enable remote root access, though this is insecure.

```
CREATE User 'root'@'%' IDENTIFIED BY '<your password>';
USE mysql;
GRANT ALL PRIVILEGES ON * to 'root'@'%' WITH GRANT OPTION;
quit
```

* Test remote access by creating a temporary mysql-client pod and logging in as root:

```
kubectl run -it --rm --image=mysql:8.0 --restart=Never mysql-client -- mysql -h mysql -p
```

    # When you see this prompt, enter your the password you created
	
	If you don't see a command prompt, try pressing enter.
	


    # You should now be logged in
	
	Welcome to the MySQL monitor.  Commands end with ; or \g.
	Your MySQL connection id is 11
	Server version: 8.0.15 MySQL Community Server - GPL
    
	Copyright (c) 2000, 2019, Oracle and/or its affiliates. All rights reserved.
    
	Oracle is a registered trademark of Oracle Corporation and/or its
	affiliates. Other names may be trademarks of their respective
	owners.
    
	Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.
    
	mysql>


##Deleting the Deployment

```
kubectl delete service mysql
kubectl delete deployment mysql
kubectl delete pvc mysql-pv-claim
kubectl delete pv mysql-pv-volume
kubectl delete secret mysql-secret

```

