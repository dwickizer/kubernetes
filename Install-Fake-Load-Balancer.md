# Creating a fake cloud load balancer

If and when you decide to create a Kubernetes LoadBalancer service, you may notice that the External-IP address shows `<pending>`. That is because it is expecting to be deployed in a public cloud where this address is normally assigned by a load balancer service (e.g. AWS ELB or ALB).

Well, in this Virtual Box environment, we haven't got one of those. So, we're going to need to fake it.

We will be using [MetalLB](https://metallb.universe.tf/). This will create a fake external load balancer within the kubernetes cluster, which will allocate IP addresses to the services via kube-proxy.


To configure it requires two simple steps:

* Deploy it:

`
kubectl apply -f https://raw.githubusercontent.com/google/metallb/v0.7.3/manifests/metallb.yaml
`

* Configure it, using [Layer 2 ARP](https://metallb.universe.tf/tutorial/layer2/)

`
mkdir -p configmaps
`

```
cat <<EOF > configmaps/metallb-config.yaml
apiVersion: v1
kind: ConfigMap
metadata:
  namespace: metallb-system
  name: config
data:
  config: |
    address-pools:
    - name: my-ip-space
      protocol: layer2
      addresses:
      - 10.96.0.200-10.96.0.210
EOF
```

**NOTE:** metallb address range on same subnet as kubernetes cluster

`
kubectl create -f configmaps/metallb-config.yaml
`


