# Creating Linked Clones for Manual Kubernetes Install


## Background

The purpose of this guide is help you deliver 3 Virtual Box linked clones (one master and two slave nodes) to support a [manual deployment of Kubernetes (K8s)](https://bitbucket.org/dwickizer/kubernetes/src/master/Manual-K8s-Install.md).

A [gold image, based on Redhat Enterprise Linux (RHEL) 7.6](https://bitbucket.org/dwickizer/kubernetes/src/master/Install-RHEL7-in-Vbox.md) will be used. It will need to be tweaked. The cluster will be installed in a private NAT Network, which will use port forwarding for access. [(That is part of the Gold Image configuration.)](https://bitbucket.org/dwickizer/kubernetes/src/master/Install-RHEL7-in-Vbox.md) Here is the outline of the process:


* Tweak Gold Image
	* Login to the Gold Image
	* Add hostnames and IP addresses to /etc/hosts
	* Configure SELinux and firewalld rules
	* Configure network bridging and IP forwarding
	* Update system & install Docker & Kubernetes
	* Disable swap
	* Create a new snapshot restore point
	

* Create Kubernetes Master Server

	* Create k8s-master as a linked clone from gold image
	* Change hostname
	* Change IP address
	* Change network device UUID
	* Reboot the node
	
	
* Create Kubernetes Nodes

	* Create each k8s-node as a linked clone from gold image
	* Change hostname
	* Change IP addresse
	* Change network device UUID
	* Reboot the node
	* Repeat as necessary
	

## Tweak Gold Image

### Login to the Gold Image

```
ssh -i ~/.ssh/my_id_rsa -p 9622 rhel@localhost
```

### Add Hostnames and IP Addresses

* Add the hostnames and IP addresses for the cluster to the bottom of /etc/hosts
 
```
sudo cat << EOF >> /etc/hosts
10.0.2.6   rhel-7.6-gold.raas.net rhel-7.6-gold
10.0.2.7   k8s-master.raas.net k8s-master
10.0.2.8   k8s-node1.raas.net kk8s-node1
10.0.2.9   k8s-node2.raas.net kk8s-node2
EOF
```


### Configure SELinux and Firewalld Rules


* Turn off SE Linux

```
sudo setenforce 0		
sudo sed -i 's/^SELINUX=enforcing$/SELINUX=permissive/' /etc/selinux/config
```

* Make sure firewalld is running and enabled; set rules to open ports for docker and k8s:

```
sudo firewall-cmd --permanent --zone=trusted --change-interface=docker0
sudo firewall-cmd --permanent --zone=trusted --add-port=4243/tcp
sudo firewall-cmd --permanent --add-port=179/tcp
sudo firewall-cmd --permanent --add-port=443/tcp
sudo firewall-cmd --permanent --add-port=2379-2380/tcp
sudo firewall-cmd --permanent --add-port=6443/tcp
sudo firewall-cmd --permanent --add-port=6783/tcp
sudo firewall-cmd --permanent --add-port=9099/tcp
sudo firewall-cmd --permanent --add-port=10250/tcp
sudo firewall-cmd --permanent --add-port=10251/tcp
sudo firewall-cmd --permanent --add-port=10252/tcp
sudo firewall-cmd --permanent --add-port=10255/tcp
sudo firewall-cmd --permanent --add-port=30000-32767/tcp
sudo firewall-cmd  --reload
```


### Configure Net.Bridge and IP Forwarding

* Some users on RHEL/CentOS 7 have reported issues with traffic being routed incorrectly due to iptables being bypassed. You should ensure net.bridge.bridge-nf-call-iptables is set to 1 in your sysctl config:

```
sudo su -c 'cat << EOF > /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOF'
```

* Configure IP Forwarding by editing /etc/sysctl.d/*-sysctl.conf and changing net.ipv4.ip_forward to 1 (ignore comments)
 
```
sudo sed -i '/^#/!s/ip_forward = 0/ip_forward = 1/g' /etc/sysctl.conf
sudo sed -i '/^#/!s/ip_forward = 0/ip_forward = 1/g' /etc/sysctl.d/*-sysctl.conf
```


### Update System & Install Docker & Kubernetes

* Update system
 
```
sudo subscription-manager repos --enable rhel-7-server-extras-rpms
sudo yum update -y && sudo yum install -y yum-utils device-mapper-persistent-data lvm2 net-tools
```

* Docker Needs To Be On All Nodes [(Here is the Reference)](https://kubernetes.io/docs/setup/cri/#docker)

* Install Docker CE Repository
 
```
sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
```

* Set and set 'notify\_only=0' in /etc/yum/pluginconf.d/search-disabled-repos.conf (ignore comments)
 
```
sudo sed -i '/^#/!s/notify_only=1/notify_only=0/g' /etc/yum/pluginconf.d/search-disabled-repos.conf
```


* Install docker ce (**NOTE:** this could take some time):
 
```
sudo yum update -y && sudo yum install -y docker-ce-18.06.1.ce
```

* Create /etc/docker directory:
 
```
sudo mkdir /etc/docker
```

* Setup docker daemon:

```  
sudo su -c 'cat << EOF > /etc/docker/daemon.json 
{
  "exec-opts": ["native.cgroupdriver=cgroupfs"],
  "log-driver": "json-file",
  "storage-driver": "overlay2",
  "storage-opts": [
    "overlay2.override_kernel_check=true"
  ]
}
EOF'
```

* Create docker service directory:
 
```
sudo mkdir -p /etc/systemd/system/docker.service.d
```


* Restart docker:
 
```
sudo systemctl daemon-reload
sudo systemctl enable docker
sudo systemctl restart docker
```

* Install kubeadm, kubelet, kubectl (required on all nodes)

```
sudo su -c 'cat << EOF > /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
exclude=kube*
EOF'
```

```
sudo yum install -y kubelet kubeadm kubectl --disableexcludes=kubernetes
sudo systemctl enable kubelet && sudo systemctl start kubelet
```


## Disable Swap

```
OLD_SWAP="`grep swap /etc/fstab`"
NEW_SWAP="# $OLD_SWAP"
```

```
sudo sed -i "s|$OLD_SWAP|$NEW_SWAP|" /etc/fstab
sudo swapoff -a
```


## Create a new snapshot restore point(Optional)

* At this point I recommend you shutdown the system and snapshot it as your k8s baseline install, just in case you have trouble with the next section and want a do-over.
 
`sudo shutdown -h now`

* Highlight your VM in the Virtual Box Window (once the system is shutdown)

* Click on the **Snapshots** button in the upper right

* Click on the **Take** button

* A modal will pop-up. Name the snapshot and provide any descriptive information you want, then click **OK**.

`Snapshot Name`		

`RHEL-7.6-k8s-Manual-Baseline-04-01-2019`

`Snapshot Description`		

```
- Added cluster hostnames and IP addresses to /etc/hosts
- Configured SELinux and firewalld rules
- Install Docker and Kubernetes common files
- Turn off swap
```



## Create Kubernetes Master Server

### Create k8s-master as a linked clone from gold image

* Right-click on the gold image VM in the Virtual Box Window and select **Clone** (it should not be running)

	* Rename it to **k8s-master**

	* Leave the box checked to reinitialize the MAC address

	* Click **Continue**
	
* Select **Linked clone** and click on **Continue**

* Select the clone you just created

* Click on the **Details** button in the upper right (or select **Details** under the **Machine Tools** button)

* Scroll down to the **Description** section and update the information (e.g., the IP address, which will be 10.0.2.7)

* Click on the **Start** button to start the VM.

* Login to the VM using ssh (remember it still has the IP of the gold image):
 
```
ssh -i $HOME/.ssh/my_id_rsa -p 9622 rhel@localhost
```


### Change hostname

```
sudo hostnamectl set-hostname k8s-master
```

### Change IP address
 
```
sudo sed -i "s/10.0.2.6/10.0.2.7/" /etc/sysconfig/network-scripts/ifcfg-enp0s3
```
### Change network device UUID

```
OLD_UUID="`sudo grep UUID /etc/sysconfig/network-scripts/ifcfg-enp0s3 | cut -d= -f2`"
NEW_UUID="`uuidgen enp0s3`" 
sudo sed -i "s/$OLD_UUID/$NEW_UUID/" /etc/sysconfig/network-scripts/ifcfg-enp0s3
```

### Reboot the node
 
```
sudo reboot
```


## Create Kubernetes Nodes

### Create each k8s-node as a linked clone from gold image

* Right-click on the gold image VM in the Virtual Box Window and select **Clone** (it should not be running)

	* Rename it to **k8s-node1**

	* Leave the box checked to reinitialize the MAC address

	* Click **Continue**
	
* Select **Linked clone** and click on **Continue**

* Select the clone you just created

* Click on the **Details** button in the upper right (or select **Details** under the **Machine Tools** button)

* Scroll down to the **Description** section and update the information (e.g., the IP address, which will be 10.0.2.8)

* Click on the **Start** button to start the VM.

* Login to the VM using ssh (remember it still has the IP of the gold image):
 
```
ssh -i $HOME/.ssh/my_id_rsa -p 9622 rhel@localhost
```

### Change hostname

```
sudo hostnamectl set-hostname k8s-node1
```

### Change IP address
 
```
sudo sed -i "s/10.0.2.6/10.0.2.8/" /etc/sysconfig/network-scripts/ifcfg-enp0s3
```

### Change network device UUID

```
OLD_UUID="`sudo grep UUID /etc/sysconfig/network-scripts/ifcfg-enp0s3 | cut -d= -f2`"
NEW_UUID="`uuidgen enp0s3`" 
sudo sed -i "s/$OLD_UUID/$NEW_UUID/" /etc/sysconfig/network-scripts/ifcfg-enp0s3
```

### Reboot the node
 
```
sudo reboot
```


### Repeat as necessary

* I did one more called **k8s-node2** at IP address **10.0.2.9**

