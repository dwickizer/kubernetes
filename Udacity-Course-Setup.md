#[Scalable Microservices with Kubernetes (Udacity)](https://www.udacity.com/course/scalable-microservices-with-kubernetes--ud615)


# Installing Go and the Udacity Class Stuff

## Install Go

	wget https://storage.googleapis.com/golang/go1.6.2.linux-amd64.tar.gz
	rm -rf /usr/local/bin/go
	sudo tar -C /usr/local -xzf go1.6.2.linux-amd64.tar.gz
	export PATH=$PATH:/usr/local/go/bin
	export GOPATH=~/go

## Get the app code

	mkdir -p $GOPATH/src/github.com/udacity
	cd $GOPATH/src/github.com/udacity
	git clone https://github.com/udacity/ud615.git

## Build a static binary of the monolith app

	cd ud615/app/monolith
	go get -u
	go build --tags netgo --ldflags '-extldflags "-lm -lstdc++ -static"'

## Build a mult-service deployment (auth, frontend & hello using nginx)

`cd $GOPATH/src/github.com/udacity/u615/kubernetes
`

**Create auth deployment:**

`kubectl create -f deployments/auth.yaml`

**Response:**

`deployment.extensions/auth created`


**Describe the deployment:**

`kubectl describe deployments auth`

**Response:**

	Name:                   auth
	Namespace:              default
	CreationTimestamp:      Fri, 25 Jan 2019 17:35:40 -0500
	Labels:                 app=auth
	                        track=stable
	Annotations:            deployment.kubernetes.io/revision: 1
	Selector:               app=auth,track=stable
	Replicas:               1 desired | 1 updated | 1 total | 1 available | 0 unavailable
	StrategyType:           RollingUpdate
	MinReadySeconds:        0
	RollingUpdateStrategy:  1 max unavailable, 1 max surge
	Pod Template:
	  Labels:  app=auth
	           track=stable
	  Containers:
	   auth:
	    Image:       udacity/example-auth:1.0.0
	    Ports:       80/TCP, 81/TCP
	    Host Ports:  0/TCP, 0/TCP
	    Limits:
	      cpu:        200m
	      memory:     10Mi
	    Liveness:     http-get http://:81/healthz delay=5s timeout=5s period=15s #success=1 #failure=3
	    Readiness:    http-get http://:81/readiness delay=5s timeout=1s period=10s #success=1 #failure=3
	    Environment:  <none>
	    Mounts:       <none>
	  Volumes:        <none>
	Conditions:
	  Type           Status  Reason
	  ----           ------  ------
	  Available      True    MinimumReplicasAvailable
	OldReplicaSets:  <none>
	NewReplicaSet:   auth-5444bc8cf5 (1/1 replicas created)
	Events:
	  Type    Reason             Age   From                   Message
	  ----    ------             ----  ----                   -------
	  Normal  ScalingReplicaSet  61s   deployment-controller  Scaled up replica set auth-5444bc8cf5 to 1
  
**Create the auth service:**

`kubectl create -f services/auth.yaml`

**Response:**

`service/auth created`

**Create hello deployment:**

`kubectl create -f deployments/hello.yaml`

**Response:**

`deployment.extensions/hello created`

**Create hello service:**

`kubectl create -f services/hello.yaml`

**Response:**

`service/hello created`

**You'll need some secrets for nginx to work properly over https:**

`kubectl create secret generic tls-certs --from-file=tls/`

**Response:**

`secret "tls-certs" created`

**Configure nginx configmap:**

`kubectl create configmap nginx-frontend-conf --from-file nginx/frontend.conf`

**Response:**

`configmap/nginx-frontend-conf created`

**Create frontend deployment:**

`kubectl create -f deployments/frontend.yaml`

**Response:**

`deployment.extensions/frontend created`

**Create frontend service:**

`kubectl create -f services/frontend.yaml`

**Response:**

`service/frontend created`

**Get frontend services IP:**

`kubectl get services frontend`

**Response:**

```
NAME       TYPE           CLUSTER-IP       EXTERNAL-IP     PORT(S)         AGE
frontend   LoadBalancer   10.105.140.123   <pending>       443:30754/TCP   21m
```

**NOTE:** See the `<pending>`? This is because the LoadBalancer is expecting to be run in a public cloud, where a real load balancer (e.g., AWS ALB) assigns an external IP. We don't have one of those. 

Therefore, we will need to improvise. We will use MetalLB. See the Kubernetes-Config-README.md for details on how to install and configure.

The net result is that it will install a configmap called **config** in the **metallb-system** namespace. It has an address pool called **my-ip-space**.

You will need to delete the **frontend** service above.

`kubectl delete service frontend`

Edit the **services/frontend.yaml** file to include an annotations line which references **my-ip-space:**

```
apiVersion: v1
kind: Service
metadata:
  name: "frontend"
  annotations:
    metallb.universe.tf/address-pool: my-ip-space
spec:
  selector:
    app: "frontend"
  ports:
    - protocol: "TCP"
      port: 443
      targetPort: 443
  type: LoadBalancer
```

**Then recreate the service:**

`kubectl create -f services/frontend.yaml`

**Now recheck the service**

`kubectl get service frontend`

**Response:**

```
NAME       TYPE           CLUSTER-IP       EXTERNAL-IP     PORT(S)         AGE
frontend     LoadBalancer   10.102.220.71    10.96.0.200   443:31330/TCP   7s
```


##Scaling

**To see the current replica sets:**

`
kubectl get replicasets
`

**Response:**

```
NAME                  DESIRED   CURRENT   READY   AGE
auth-5444bc8cf5       1         1         1       26m
frontend-569465456f   1         1         1       25m
hello-6f46cfb99c      1         1         1       26m
```

**Check to see how many hello pods are running:**

`kubectl get pods -l "app=hello,track=stable"`

**Response:**

```
NAME                     READY   STATUS    RESTARTS   AGE
hello-6f46cfb99c-hwpd2   1/1     Running   1          45m
```


**Change the desired number of replicas to 3:**

`sed -i "s/replicas: 1/replicas: 3/" deployments/hello.yaml`

**Redeploy:**

`kubectl apply -f deployments/hello.yaml`

**Response:**

`deployment.extensions/hello configured`

**Recheck replicasets:**

`kubectl get replicasets`

**Response:**

```
NAME                  DESIRED   CURRENT   READY   AGE
auth-5444bc8cf5       1         1         1       52m
frontend-569465456f   1         1         1       51m
hello-6f46cfb99c      3         3         3       52m
```

**Check the number of pods running:**

`kubectl get pods`

**Response:**

```
NAME                        READY   STATUS    RESTARTS   AGE
auth-5444bc8cf5-599df       1/1     Running   1          53m
frontend-569465456f-zpn4p   1/1     Running   2          52m
hello-6f46cfb99c-hwpd2      1/1     Running   1          53m
hello-6f46cfb99c-p6z49      1/1     Running   0          2m59s
hello-6f46cfb99c-qjn92      1/1     Running   0          2m59s
```


##Rolling updates

**Simply edit one of the deployment files to update the image version.**

**Re-apply the deployment file with kubectl.**

**Watch the progress of the rolling update using the kubectl describe deployment and kubectl get pods commands.**





