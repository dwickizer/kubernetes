# **Kubernetes Deployment Using KubeSpray**

_**[Kubespray Reference](https://kubespray.io/#/)**_

## **Purpose:**

Document the automated deployment of a 3-node Kubernetes (k8s) Cluster Using Ansible


## **Assumptions:**

* This is being built as a prototype for "iron huggers" who don't trust public cloud

* The starting point will be one (1) running RHEL7 node/VM configured as the Ansible Master and three (3) running, newly installed RHEL7 nodes/VMs, which will serve as the k8s cluster. In my case these will be created from [linked clones from a RHEL7 Gold Image](https://bitbucket.org/dwickizer/kubernetes/src/master/kubespray/Linked-Clones-for-Kubespray-Anisble-Install-README.md).

* Since I am using Virtual Box for this I used the following HW configs for each of the VMs (to keep memory within reason):
    * **ansible-svr node:** 1 core, 1024 MB RAM
	* **k8s-nodes:** 2 cores, 2048 MB RAM

* The highly available k8s cluster will consist of the following node configuration:
	* 3 nodes
	* Redundant system pods distributed across the nodes
	
		* 2 master controller managers
		* 2 api servers
		* 2 core dns servers
		* 3 kube-proxy servers
		* 2 schedulers

* The software defined network (SDN) (a.k.a, container networking interface or CNI) used for the pods will employ the **calico** plug-in. Of the several plug-ins supported, this one seems to be the best balance of security, performance, simplicity and resource utilization, based on information from [this assessment](https://itnext.io/benchmark-results-of-kubernetes-network-plugins-cni-over-10gbit-s-network-36475925a560). It also happens to be the default.


## **Starting Point:**

* All linked clones should be up and running with RHEL7

* From your system, copy the private ssh key you will use to the ~/.ssh directory for rhel user on what will be the Ansible server (in my case the key is called ansible\_id\_rsa).

```
scp -i ~/.ssh/ansible_id_rsa -P 9622 ~/.ssh/ansible_id_rsa rhel@localhost:./.ssh
```

* Log into the ansible-svr node:

```
ssh -i ~/.ssh/ansible_id_rsa -p 9622 rhel@localhost
```


* On what is to be the Ansible node:

```
sudo mkdir -p /opt/ansible
sudo chown rhel:rhel /opt/ansible
cd /opt/ansible
```

* Install Python3 and create its own virtualenv from it.

```
sudo subscription-manager repos --enable rhel-7-server-optional-rpms  --enable rhel-server-rhscl-7-rpms
sudo yum -y install @development
sudo yum -y install rh-python36
```

```
scl enable rh-python36 bash
```

```
python3 -m venv env
```

* Need to ensure these pip packages are installed as well:

```
. /opt/ansible/env/bin/activate
pip install -U pip 
pip install wheel ruamel.yaml
```

* Clone git repository (specific version)

```
git clone https://github.com/kubernetes-sigs/kubespray.git
cd kubespray
```

## **Build Cluster::**

* Install dependencies from ``requirements.txt``. This will install Ansible as well.

```
pip install -r requirements.txt
```


* Copy ``inventory/sample`` as ``inventory/mycluster``

```
cp -rfp inventory/sample inventory/mycluster
```

* Update Ansible inventory file with inventory builder

```
declare -a IPS=(10.0.2.7 10.0.2.8 10.0.2.9)
CONFIG_FILE=inventory/mycluster/hosts.yaml python3 contrib/inventory_builder/inventory.py ${IPS[@]}
```

* Review and change parameters under ``inventory/mycluster/group_vars``
```
less inventory/mycluster/group_vars/all/all.yml
less inventory/mycluster/group_vars/k8s-cluster/k8s-cluster.yml
```

* For performance, change the MTU for the calico CNI to 9000 bytes

```
sed -i "s/# calico_mtu: 1500/calico_mtu: 9000/" roles/network_plugin/calico/defaults/main.yml
```


* Add the following to the ansible.cfg file under [defaults] so that the Ansible server will be able to communicate using a private key

```
inventory: /opt/ansible/kubespray/inventory/mycluster/hosts.yaml
remote_user: rhel
private_key_file: ~/.ssh/ansible_id_rsa
```



* Deploy Kubespray with Ansible Playbook - run the playbook as root

**NOTE:** The option `-b` is required, as for example writing SSL keys in /etc/, installing packages and interacting with various systemd daemons.
Without -b the playbook will fail to run!

**This process will take about 25 minutes.**

```
ansible-playbook -i inventory/mycluster/hosts.yaml --become --become-user=root cluster.yml
```

## **Test Cluster**

* On the ansible-sver, create ssh config file (as rhel user) to simplify logging into nodes


```
cat << EOL > ~/.ssh/config
Host k8s-node1
    HostName node1
    User rhel
    Port 22
    IdentityFile ~/.ssh/ansible_id_rsa
Host k8s-node2
    HostName node2
    User rhel
    Port 22
    IdentityFile ~/.ssh/ansible_id_rsa
Host k8s-node3
    HostName node3
    User rhel
    Port 22
    IdentityFile ~/.ssh/ansible_id_rsa
EOL
```

```
chmod 644 ~/.ssh/config
```                                                    

* Login 1st master

```
ssh k8s-node1
```

* Enable non-root user to manage the cluster

``` 
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
```

* Check that all nodes are ready

```
kubectl get nodes -o wide 
```

```
    # Output should look something like this:
```
	
```
	NAME    STATUS   ROLES         AGE   VERSION   INTERNAL-IP   EXTERNAL-IP   OS-IMAGE                   KERNEL-VERSION               CONTAINER-RUNTIME
	node1   Ready    master,node   11m   v1.13.5   10.0.2.7      <none>        Red Hat Enterprise Linux   3.10.0-957.10.1.el7.x86_64   docker://18.6.2
	node2   Ready    master,node   10m   v1.13.5   10.0.2.8      <none>        Red Hat Enterprise Linux   3.10.0-957.10.1.el7.x86_64   docker://18.6.2
	node3   Ready    node          10m   v1.13.5   10.0.2.9      <none>        Red Hat Enterprise Linux   3.10.0-957.10.1.el7.x86_64   docker://18.6.2
```


* Check that all system pods are up and running

```
kubectl get pods -o wide --all-namespaces
```

```
	# Output should look something like this:
```
	
```
	NAMESPACE     NAME                                       READY   STATUS    RESTARTS   AGE     IP            NODE    NOMINATED NODE   READINESS GATES
	kube-system   calico-kube-controllers-76878758f6-rtghn   1/1     Running   0          9m31s   10.0.2.9      node3   <none>           <none>
	kube-system   calico-node-7qd8g                          1/1     Running   0          9m45s   10.0.2.8      node2   <none>           <none>
	kube-system   calico-node-rgp6t                          1/1     Running   0          9m45s   10.0.2.9      node3   <none>           <none>
	kube-system   calico-node-sw8tl                          1/1     Running   0          9m45s   10.0.2.7      node1   <none>           <none>
	kube-system   coredns-644c686c9-k6w9q                    1/1     Running   0          9m3s    10.233.90.1   node1   <none>           <none>
	kube-system   coredns-644c686c9-mkrvb                    1/1     Running   0          8m56s   10.233.96.2   node2   <none>           <none>
	kube-system   dns-autoscaler-586f58b8bf-mssq4            1/1     Running   0          8m58s   10.233.96.1   node2   <none>           <none>
	kube-system   kube-apiserver-node1                       1/1     Running   0          11m     10.0.2.7      node1   <none>           <none>
	kube-system   kube-apiserver-node2                       1/1     Running   0          11m     10.0.2.8      node2   <none>           <none>
	kube-system   kube-controller-manager-node1              1/1     Running   0          11m     10.0.2.7      node1   <none>           <none>
	kube-system   kube-controller-manager-node2              1/1     Running   0          11m     10.0.2.8      node2   <none>           <none>
	kube-system   kube-proxy-bxx2p                           1/1     Running   0          9m31s   10.0.2.7      node1   <none>           <none>
	kube-system   kube-proxy-j46s7                           1/1     Running   0          9m48s   10.0.2.8      node2   <none>           <none>
	kube-system   kube-proxy-r7lkq                           1/1     Running   0          9m36s   10.0.2.9      node3   <none>           <none>
	kube-system   kube-scheduler-node1                       1/1     Running   0          11m     10.0.2.7      node1   <none>           <none>
	kube-system   kube-scheduler-node2                       1/1     Running   0          11m     10.0.2.8      node2   <none>           <none>
	kube-system   kubernetes-dashboard-8457c55f89-qw75x      1/1     Running   0          8m54s   10.233.92.3   node3   <none>           <none>
	kube-system   nginx-proxy-node3                          1/1     Running   0          10m     10.0.2.9      node3   <none>           <none>
```

* Check to see what services are running

```
kubectl get services -o wide --all-namespaces
```

```
    # Output should look something like this:
```
	
```
	NAMESPACE     NAME                   TYPE        CLUSTER-IP     EXTERNAL-IP   PORT(S)                  AGE   SELECTOR
	default       kubernetes             ClusterIP   10.233.0.1     <none>        443/TCP                  25m   <none>
	kube-system   coredns                ClusterIP   10.233.0.3     <none>        53/UDP,53/TCP,9153/TCP   20m   k8s-app=coredns
	kube-system   kubernetes-dashboard   ClusterIP   10.233.58.12   <none>        443/TCP                  20m   k8s-app=kubernetes-dashboard
```


