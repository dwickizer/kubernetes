# Linked Clones for Kubespray Ansible Install


## Background

* The purpose of this README is to walk you through how to modify a [gold image, based on Redhat Enterprise Linux (RHEL) 7.6](https://bitbucket.org/dwickizer/kubernetes/src/master/Install-RHEL7-in-Vbox.md) and create and configure four (4) linked clones from that image. The image will need to be tweaked. The cluster will be installed in a private NAT Network, which will use port forwarding for access. [(That is part of the Gold Image configuration.)](https://bitbucket.org/dwickizer/kubernetes/src/master/Install-RHEL7-in-Vbox.md) Here is the outline of the process:

* Tweak Gold Image

	* Login to the Gold Image
	* Add hostnames and IP addresses to /etc/hosts
	* Enable Extras in Subscription Manager
	* Configure SELinux and firewalld rules
	* Configure network bridging and IP forwarding
	* Create a new snapshot restore point

* Create Kubernetes Node1

	* Create node1 as a linked clone from gold image
	* Change hostname
	* Change IP address
	* Change network device UUID
	* Reboot node
	
* Create Kubernetes Node2

	* Create each node2 as a linked clone from gold image
	* Change hostname
	* Change IP address
	* Change network device UUID
	* Reboot node
	* Repeat as necessary

* Create Kubernetes Node3

	* Create each node3 as a linked clone from gold image
	* Change hostname
	* Change IP address
	* Change network device UUID
	* Reboot node
	* Repeat as necessary

* Create Linked Clone for Ansible Server

	* Create ansible-svr as a linked clone from gold image
	* Change hostname
	* Reboot node


## Tweak Gold Image

### Login to the Gold Image

```
ssh -i ~/.ssh/my_id_rsa -p 9622 rhel@localhost
```

### Add Hostnames and IP Addresses

* Add the hostnames and IP addresses for the cluster to the bottom of /etc/hosts
 
```
sudo su -c 'cat << EOF >> /etc/hosts
10.0.2.6   ansible-svr.raas.net ansible-svr
10.0.2.7   node1.raas.net node1
10.0.2.8   node2.raas.net node2
10.0.2.9   node3.raas.net node3
EOF'
```

### Enable Extras in Subscription Manager

```
sudo subscription-manager repos --enable rhel-7-server-extras-rpms
sudo yum update -y
```

### Configure SELinux and Firewalld Rules


* Turn off SE Linux

```
sudo setenforce 0
sudo sed -i 's/^SELINUX=enforcing$/SELINUX=permissive/' /etc/selinux/config
```

* Make sure firewalld is running and enabled; set rules to open ports for k8s:

```
sudo systemctl restart firewalld
sudo systemctl enable firewalld
sudo firewall-cmd --permanent --add-port=179/tcp
sudo firewall-cmd --permanent --add-port=443/tcp
sudo firewall-cmd --permanent --add-port=2379-2380/tcp
sudo firewall-cmd --permanent --add-port=6443/tcp
sudo firewall-cmd --permanent --add-port=6783/tcp
sudo firewall-cmd --permanent --add-port=9099/tcp
sudo firewall-cmd --permanent --add-port=10250/tcp
sudo firewall-cmd --permanent --add-port=10251/tcp
sudo firewall-cmd --permanent --add-port=10252/tcp
sudo firewall-cmd --permanent --add-port=10255/tcp
sudo firewall-cmd --permanent --add-port=30000-32767/tcp
sudo firewall-cmd  --reload
```


### Configure Net.Bridge and IP Forwarding

* Some users on RHEL/CentOS 7 have reported issues with traffic being routed incorrectly due to iptables being bypassed. You should ensure net.bridge.bridge-nf-call-iptables is set to 1 in your sysctl config:

```
sudo su -c 'cat <<EOF >  /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOF'
```

* Configure IP Forwarding by editing /etc/sysctl.d/*-sysctl.conf and changing net.ipv4.ip_forward to 1 (ignore comments)
 
```
sudo sed -i '/^#/!s/ip_forward = 0/ip_forward = 1/g' /etc/sysctl.conf
sudo sed -i '/^#/!s/ip_forward = 0/ip_forward = 1/g' /etc/sysctl.d/*-sysctl.conf
```


### Create a new snapshot restore point(Optional)

* At this point I recommend you shutdown the system and snapshot it as your k8s baseline install, just in case you have trouble with the next section and want a do-over.
 
```
sudo shutdown -h now
```

* Highlight your VM in the Virtual Box Window (once the system is shutdown)

* Click on the **Snapshots** button in the upper right

* Click on the **Take** button

* A modal will pop-up. Name the snapshot and provide any descriptive information you want, then click **OK**.

`Snapshot Name`		

`RHEL-7.6-k8s-Kubespray-Baseline-04-01-2019`

`Snapshot Description`		

```
- Added cluster hostnames and IP addresses to /etc/hosts
- Configured SELinux and firewalld rules
```


## Create linked clone for node1

* Right-click on the gold image VM in the Virtual Box Window and select **Clone** (it should not be running)

	* Rename it to **k8s-node.1**

	* Leave the box checked to reinitialize the MAC address

	* Click **Continue**
	
* Select **Linked clone** and click on **Continue**

* Select the clone you just created

* Click on the **Details** button in the upper right (or select **Details** under the **Machine Tools** button)

* Scroll down to the **Description** section and update the information (e.g., the IP address, which will be 10.0.2.7)

* Click on the **Start** button to start the VM.

* Login to the VM using ssh (remember it still has the IP of the gold image):
 
```
ssh -i $HOME/.ssh/my_id_rsa -p 9622 rhel@localhost
```


### Change hostname

```
sudo hostnamectl set-hostname node1
```

### Change IP address
 
```
sudo sed -i "s/10.0.2.6/10.0.2.7/" /etc/sysconfig/network-scripts/ifcfg-enp0s3
```

### Change network device UUID

```
OLD_UUID="`sudo grep UUID /etc/sysconfig/network-scripts/ifcfg-enp0s3 | cut -d= -f2`"
NEW_UUID="`uuidgen enp0s3`" 
sudo sed -i "s/$OLD_UUID/$NEW_UUID/" /etc/sysconfig/network-scripts/ifcfg-enp0s3
```

### Reboot node
 
```
sudo reboot
```


## Create linked clone for node2

* Right-click on the gold image VM in the Virtual Box Window and select **Clone** (it should not be running)

	* Rename it to **k8s-node.2**

	* Leave the box checked to reinitialize the MAC address

	* Click **Continue**
	
* Select **Linked clone** and click on **Continue**

* Select the clone you just created

* Click on the **Details** button in the upper right (or select **Details** under the **Machine Tools** button)

* Scroll down to the **Description** section and update the information (e.g., the IP address, which will be 10.0.2.8)

* Click on the **Start** button to start the VM.

* Login to the VM using ssh (remember it still has the IP of the gold image):
 
```
ssh -i $HOME/.ssh/my_id_rsa -p 9622 rhel@localhost
```

### Change hostname

```
sudo hostnamectl set-hostname node2
```

### Change IP address
 
```
sudo sed -i "s/10.0.2.6/10.0.2.8/" /etc/sysconfig/network-scripts/ifcfg-enp0s3
```

### Change network device UUID

```
OLD_UUID="`sudo grep UUID /etc/sysconfig/network-scripts/ifcfg-enp0s3 | cut -d= -f2`"
NEW_UUID="`uuidgen enp0s3`" 
sudo sed -i "s/$OLD_UUID/$NEW_UUID/" /etc/sysconfig/network-scripts/ifcfg-enp0s3
```

### Reboot node
 
```
sudo reboot
```

## Create linked clone for node3

* Right-click on the gold image VM in the Virtual Box Window and select **Clone** (it should not be running)

	* Rename it to **k8s-node.3**

	* Leave the box checked to reinitialize the MAC address

	* Click **Continue**
	
* Select **Linked clone** and click on **Continue**

* Select the clone you just created

* Click on the **Details** button in the upper right (or select **Details** under the **Machine Tools** button)

* Scroll down to the **Description** section and update the information (e.g., the IP address, which will be 10.0.2.8)

* Click on the **Start** button to start the VM.

* Login to the VM using ssh (remember it still has the IP of the gold image):
 
```
ssh -i $HOME/.ssh/my_id_rsa -p 9622 rhel@localhost
```

### Change hostname

```
sudo hostnamectl set-hostname node3
```

### Change IP address
 
```
sudo sed -i "s/10.0.2.6/10.0.2.9/" /etc/sysconfig/network-scripts/ifcfg-enp0s3
```

### Change network device UUID

```
OLD_UUID="`sudo grep UUID /etc/sysconfig/network-scripts/ifcfg-enp0s3 | cut -d= -f2`"
NEW_UUID="`uuidgen enp0s3`" 
sudo sed -i "s/$OLD_UUID/$NEW_UUID/" /etc/sysconfig/network-scripts/ifcfg-enp0s3
```

### Reboot node
 
```
sudo reboot
```


## Create linked clone for ansible-svr

* Right-click on the gold image VM in the Virtual Box Window and select **Clone** (it should not be running)

	* Rename it to **ansible-svr**

	* Leave the box checked to reinitialize the MAC address

	* Click **Continue**
	
* Select **Linked clone** and click on **Continue**

* Select the clone you just created

* Click on the **Details** button in the upper right (or select **Details** under the **Machine Tools** button)

* Click on the **Start** button to start the VM.

* Login to the VM using ssh (remember it still has the IP of the gold image):
 
```
ssh -i $HOME/.ssh/my_id_rsa -p 9622 rhel@localhost
```


### Change hostname

```
sudo hostnamectl set-hostname ansible-svr
```


### Reboot node
 
```
sudo reboot
```



